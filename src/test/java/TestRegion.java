import org.junit.Test;
import org.ximinghui.region.china.Region;

public class TestRegion {

    @Test
    public void test() {
        System.out.println("山南是一级区域吗：" + Region.SHANNAN.is1stLevelDivision());
        System.out.println("山南是二级区域吗：" + Region.SHANNAN.is2ndLevelDivision());
        System.out.println("山南的类别是：" + Region.SHANNAN.category.chineseName);
        System.out.println("山南属于西藏吗：" + Region.SHANNAN.isBelongTo(Region.TIBET));
    }

}

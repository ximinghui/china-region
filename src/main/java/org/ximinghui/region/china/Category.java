package org.ximinghui.region.china;

import lombok.AllArgsConstructor;

/**
 * 地区类别
 * <p>
 * 参考：
 * <ol>
 * <li><a href="http://www.gov.cn/guoqing/2005-09/13/content_5043917.htm">中华人民共和国行政区划_国情相关_中国政府网</a></li>
 * <li><a href="https://baike.baidu.com/item/%E4%B8%AD%E5%8D%8E%E4%BA%BA%E6%B0%91%E5%85%B1%E5%92%8C%E5%9B%BD%E8%A1%8C%E6%94%BF%E5%8C%BA%E5%88%92">中华人民共和国行政区划_百度百科</a></li>
 * <li><a href="https://en.wikipedia.org/wiki/Administrative_divisions_of_China">Administrative divisions of China - Wikipedia</a></li>
 * </ol>
 */
@AllArgsConstructor
@SuppressWarnings("unused")
public enum Category {

    /**
     * 省（一级）
     */
    PROVINCE(Level.PROVINCIAL, "省"),

    /**
     * 自治区（一级）
     */
    AUTONOMOUS_REGION(Level.PROVINCIAL, "自治区"),

    /**
     * 直辖市（一级）
     */
    MUNICIPALITY(Level.PROVINCIAL, "直辖市"),

    /**
     * 特别行政区（一级）
     */
    SPECIAL_ADMINISTRATIVE_REGION(Level.PROVINCIAL, "特别行政区"),

    /**
     * 地区（二级）
     */
    PREFECTURE(Level.PREFECTURAL, "地区"),

    /**
     * 自治州（二级）
     */
    AUTONOMOUS_PREFECTURE(Level.PREFECTURAL, "自治州"),

    /**
     * 地级市（二级）
     */
    PREFECTURE_LEVEL_CITY(Level.PREFECTURAL, "地级市"),

    /**
     * 盟（二级）
     */
    LEAGUE(Level.PREFECTURAL, "盟"),

    /**
     * 县（三级）
     */
    COUNTY(Level.COUNTY, "县"),

    /**
     * 自治县（三级）
     */
    AUTONOMOUS_COUNTY(Level.COUNTY, "自治县"),

    /**
     * 县级市（三级）
     */
    COUNTY_LEVEL_CITY(Level.COUNTY, "县级市"),

    /**
     * 市辖区/区（三级）
     */
    DISTRICT(Level.COUNTY, "市辖区/区"),

    /**
     * 旗（三级）
     */
    BANNER(Level.COUNTY, "旗"),

    /**
     * 自治旗（三级）
     */
    AUTONOMOUS_BANNER(Level.COUNTY, "自治旗"),

    /**
     * 林区（三级）
     */
    FORESTRY_AREA(Level.COUNTY, "林区"),

    /**
     * 特区（三级）
     */
    SPECIAL_DISTRICT(Level.COUNTY, "特区"),

    /**
     * 街道/街（四级）
     */
    SUBDISTRICT(Level.TOWNSHIP, "街道/街"),

    /**
     * 乡（四级）
     */
    TOWNSHIP(Level.TOWNSHIP, "乡"),

    /**
     * 县辖区（四级）
     */
    COUNTY_CONTROLLED_DISTRICT(Level.TOWNSHIP, "县辖区"),

    /**
     * 民族乡（四级）
     */
    ETHNIC_TOWNSHIP(Level.TOWNSHIP, "民族乡"),

    /**
     * 苏木（四级）
     */
    SUM(Level.TOWNSHIP, "苏木"),

    /**
     * 民族苏木（四级）
     */
    ETHNIC_SUM(Level.TOWNSHIP, "民族苏木");

    /**
     * 地区类别对应级别
     */
    public final Level level;

    /**
     * 地区类别中文名
     */
    public final String chineseName;

}

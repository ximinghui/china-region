package org.ximinghui.region.china;

import lombok.AllArgsConstructor;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.ximinghui.region.china.AdministrativeDivision.*;
import static org.ximinghui.region.china.Category.*;

/**
 * 区域枚举
 */
@AllArgsConstructor
@SuppressWarnings({"unused", "deprecation"})
public enum Region {

    /**
     * 北京市
     */
    BEIJING(null, MUNICIPALITY, BJ.administrativeDivisionName, BJ),

    /**
     * 天津市
     */
    TIANJIN(null, MUNICIPALITY, TJ.administrativeDivisionName, TJ),

    /**
     * 河北省
     */
    HEBEI_PROVINCE(null, PROVINCE, HE.administrativeDivisionName, HE),

    /**
     * 山西省
     */
    SHANXI_PROVINCE(null, PROVINCE, SX.administrativeDivisionName, SX),

    /**
     * 内蒙古自治区
     */
    INNER_MONGOLIA(null, AUTONOMOUS_REGION, NM.administrativeDivisionName, NM),

    /**
     * 辽宁省
     */
    LIAONING_PROVINCE(null, PROVINCE, LN.administrativeDivisionName, LN),

    /**
     * 吉林省
     */
    JILIN_PROVINCE(null, PROVINCE, JL.administrativeDivisionName, JL),

    /**
     * 黑龙江省
     */
    HEILONGJIANG_PROVINCE(null, PROVINCE, HL.administrativeDivisionName, HL),

    /**
     * 上海市
     */
    SHANGHAI(null, MUNICIPALITY, SH.administrativeDivisionName, SH),

    /**
     * 江苏省
     */
    JIANGSU_PROVINCE(null, MUNICIPALITY, JS.administrativeDivisionName, JS),

    /**
     * 浙江省
     */
    ZHEJIANG_PROVINCE(null, MUNICIPALITY, ZJ.administrativeDivisionName, ZJ),

    /**
     * 安徽省
     */
    ANHUI_PROVINCE(null, MUNICIPALITY, AH.administrativeDivisionName, AH),

    /**
     * 福建省
     */
    FUJIAN_PROVINCE(null, MUNICIPALITY, FJ.administrativeDivisionName, FJ),

    /**
     * 江西省
     */
    JIANGXI_PROVINCE(null, MUNICIPALITY, JX.administrativeDivisionName, JX),

    /**
     * 山东省
     */
    SHANDONG_PROVINCE(null, MUNICIPALITY, SD.administrativeDivisionName, SD),

    /**
     * 河南省
     */
    HENAN_PROVINCE(null, PROVINCE, HA.administrativeDivisionName, HA),

    /**
     * 湖北省
     */
    HUBEI_PROVINCE(null, PROVINCE, HB.administrativeDivisionName, HB),

    /**
     * 湖南省
     */
    HUNAN_PROVINCE(null, PROVINCE, HN.administrativeDivisionName, HN),

    /**
     * 广东省
     */
    GUANGDONG_PROVINCE(null, PROVINCE, GD.administrativeDivisionName, GD),

    /**
     * 广西壮族自治区
     */
    GUANGXI_ZHUANG(null, AUTONOMOUS_REGION, GX.administrativeDivisionName, GX),

    /**
     * 海南省
     */
    HAINAN_PROVINCE(null, AUTONOMOUS_REGION, HI.administrativeDivisionName, HI),

    /**
     * 重庆市
     */
    CHONGQING(null, MUNICIPALITY, CQ.administrativeDivisionName, CQ),

    /**
     * 四川省
     */
    SICHUAN_PROVINCE(null, MUNICIPALITY, SC.administrativeDivisionName, SC),

    /**
     * 贵州省
     */
    GUIZHOU_PROVINCE(null, MUNICIPALITY, GZ.administrativeDivisionName, GZ),

    /**
     * 云南省
     */
    YUNNAN_PROVINCE(null, MUNICIPALITY, YN.administrativeDivisionName, YN),

    /**
     * 西藏自治区
     */
    TIBET(null, AUTONOMOUS_REGION, XZ.administrativeDivisionName, XZ),

    /**
     * 陕西省
     */
    SHAANXI_PROVINCE(null, AUTONOMOUS_REGION, SN.administrativeDivisionName, SN),

    /**
     * 甘肃省
     */
    GANSU_PROVINCE(null, AUTONOMOUS_REGION, GS.administrativeDivisionName, GS),

    /**
     * 青海省
     */
    QINGHAI_PROVINCE(null, AUTONOMOUS_REGION, QH.administrativeDivisionName, QH),

    /**
     * 宁夏回族自治区
     */
    NINGXIA_HUI(null, AUTONOMOUS_REGION, NX.administrativeDivisionName, NX),

    /**
     * 新疆维吾尔自治区
     */
    XINJIANG_UYGUR(null, AUTONOMOUS_REGION, XJ.administrativeDivisionName, XJ),

    /**
     * 台湾省
     */
    TAIWAN_PROVINCE(null, PROVINCE, TW.administrativeDivisionName, TW),

    /**
     * 香港特别行政区
     */
    HONG_KONG(null, SPECIAL_ADMINISTRATIVE_REGION, HK.administrativeDivisionName, HK),

    /**
     * 澳门特别行政区
     */
    MACAU(null, SPECIAL_ADMINISTRATIVE_REGION, MO.administrativeDivisionName, MO),

    // 北京市下地级（即“二级”）区域
    // 注：直辖市的“区”与地级市、地区、盟、自治州同级（即为“二级”区域）
    /**
     * 北京市辖区
     */
    _BEIJING_MUNICIPAL_DISTRICT(BEIJING, null, _BJ_MUNICIPAL_DISTRICT.administrativeDivisionName, _BJ_MUNICIPAL_DISTRICT),

    /**
     * 北京市 东城区
     */
    DONGCHENG(BEIJING, PREFECTURE, DCQ.administrativeDivisionName, DCQ),

    /**
     * 北京市 西城区
     */
    XICHENG(BEIJING, PREFECTURE, XCQ.administrativeDivisionName, XCQ),

    /**
     * 北京市 朝阳区
     */
    CHAOYANG(BEIJING, PREFECTURE, CYQ.administrativeDivisionName, CYQ),

    /**
     * 北京市 丰台区
     */
    FENGTAI(BEIJING, PREFECTURE, FTQ.administrativeDivisionName, FTQ),

    /**
     * 北京市 石景山区
     */
    SHIJINGSHAN(BEIJING, PREFECTURE, SJS.administrativeDivisionName, SJS),

    /**
     * 北京市 海淀区
     */
    HAIDIAN(BEIJING, PREFECTURE, HDN.administrativeDivisionName, HDN),

    /**
     * 北京市 门头沟区
     */
    MENTOUGOU(BEIJING, PREFECTURE, MTG.administrativeDivisionName, MTG),

    /**
     * 北京市 房山区
     */
    FANGSHAN(BEIJING, PREFECTURE, FSQ.administrativeDivisionName, FSQ),

    /**
     * 北京市 通州区
     */
    TONGZHOU(BEIJING, PREFECTURE, TZQ.administrativeDivisionName, TZQ),

    /**
     * 北京市 顺义区
     */
    SHUNYI(BEIJING, PREFECTURE, SYI.administrativeDivisionName, SYI),

    /**
     * 北京市 昌平区
     */
    CHANGPING(BEIJING, PREFECTURE, CHP.administrativeDivisionName, CHP),

    /**
     * 北京市 大兴区
     */
    DAXING(BEIJING, PREFECTURE, DXU.administrativeDivisionName, DXU),

    /**
     * 北京市 怀柔区
     */
    HUAIROU(BEIJING, PREFECTURE, HRO.administrativeDivisionName, HRO),

    /**
     * 北京市 平谷区
     */
    PINGGU(BEIJING, PREFECTURE, PGU.administrativeDivisionName, PGU),

    /**
     * 北京市 密云区
     */
    MIYUN(BEIJING, PREFECTURE, /* MYN.administrativeDivisionName */ "密云区", MYN),

    /**
     * 北京市 延庆区
     */
    YANQING(BEIJING, PREFECTURE, /* YQX.administrativeDivisionName */ "延庆区", YQX),

    // 西藏自治区下地级（即“二级”）区域
    /**
     * 拉萨市
     * <p>
     * 藏语：ལྷ་ས་གྲོང་ཁྱེར།
     */
    LHASA(TIBET, PREFECTURE_LEVEL_CITY, LXA.administrativeDivisionName, LXA),

    /**
     * 日喀则市
     * <p>
     * 藏语：གཞིས་ཀ་རྩེ་གྲོང་ཁྱེར།
     */
    SHIGATSE(TIBET, PREFECTURE_LEVEL_CITY, /* XID.administrativeDivisionName */ "日喀则市", XID),

    /**
     * 山南市
     * <p>
     * 藏语：ལྷོ་ཁ་གྲོང་ཁྱེར།
     */
    SHANNAN(TIBET, PREFECTURE_LEVEL_CITY, /* SND.administrativeDivisionName */ "山南市", SND),

    /**
     * 林芝市
     * <p>
     * 藏语：ཉིང་ཁྲི་གྲོང་ཁྱེར།
     */
    NYINGCHI(TIBET, PREFECTURE_LEVEL_CITY, /* NYD.administrativeDivisionName */ "林芝市", NYD),

    /**
     * 昌都市
     * <p>
     * 藏语：ཆབ་མདོ་གྲོང་ཁྱེར།
     */
    CHAMDO(TIBET, PREFECTURE_LEVEL_CITY, /* QAD.administrativeDivisionName */ "昌都市", QAD),

    /**
     * 那曲市
     * <p>
     * 藏语：ནག་ཆུ་གྲོང་ཁྱེར།
     */
    NAGQU(TIBET, PREFECTURE_LEVEL_CITY, /* NAD.administrativeDivisionName */ "那曲市", NAD),

    /**
     * 阿里地区
     * <p>
     * 藏语：མངའ་རིས་ས་ཁུལ།
     */
    NGARI(TIBET, PREFECTURE, NGD.administrativeDivisionName, NGD),

    // 西藏自治区下县级（即“三级”）区域
    /**
     * 拉萨市辖区
     */
    _LHASA_MUNICIPAL_DISTRICT(TIBET, null, _LXA_MUNICIPAL_DISTRICT.administrativeDivisionName, _LXA_MUNICIPAL_DISTRICT),

    /**
     * 拉萨市 城关区
     * <p>
     * 藏语：ལྷ་ས་གྲོང་ཁྱེར། ལྷ་ས་གྲོང་ཁྱེར།
     */
    CHENGGUAN(LHASA, DISTRICT, CGN.administrativeDivisionName, CGN),

    /**
     * 拉萨市 堆龙德庆区
     * <p>
     * 藏语：ལྷ་ས་གྲོང་ཁྱེར། སྟོད་ལུང་བདེ་ཆེན་ཆུས།<br>
     * 外语：Doilungdêqên
     */
    DOILUNGDEQEN(LHASA, DISTRICT, /* DOI.administrativeDivisionName */ "堆龙德庆区", DOI),

    /**
     * 拉萨市 达孜区
     * <p>
     * 藏语：ལྷ་ས་གྲོང་ཁྱེར། སྟག་རྩེ་ཆུས།<br>
     * 外语：Dagzê
     */
    DAGZE(LHASA, DISTRICT, /* DAG.administrativeDivisionName */ "达孜区", DAG),

    /**
     * 拉萨市 林周县
     * <p>
     * 藏语：ལྷ་ས་གྲོང་ཁྱེར། ལྷུན་གྲུབ་རྫོང་།།<br>
     * 外语：Lhünzhub
     */
    LHUNZHUB(LHASA, COUNTY, LZB.administrativeDivisionName, LZB),

    /**
     * 拉萨市 当雄县
     * <p>
     * 藏语：ལྷ་ས་གྲོང་ཁྱེར། འདམ་གཞུང་རྫོང་།
     */
    DAMXUNG(LHASA, COUNTY, DAM.administrativeDivisionName, DAM),

    /**
     * 拉萨市 尼木县
     * <p>
     * 藏语：ལྷ་ས་གྲོང་ཁྱེར། སྙེ་མོ་རྫོང་།<br>
     * 外语：Nyêmo
     */
    NYEMO(LHASA, COUNTY, NYE.administrativeDivisionName, NYE),

    /**
     * 拉萨市 曲水县
     * <p>
     * 藏语：ལྷ་ས་གྲོང་ཁྱེར། ཆུ་ཤུར་རྫོང་།<br>
     * 外语：Qüxü
     */
    QUXU(LHASA, COUNTY, QUX.administrativeDivisionName, QUX),

    /**
     * 拉萨市 墨竹工卡县
     * <p>
     * 藏语：ལྷ་ས་གྲོང་ཁྱེར། མལ་གྲོ་གུང་དཀར་རྫོང་།
     */
    MAIZHOKUNGGAR(LHASA, COUNTY, MAI.administrativeDivisionName, MAI),

    /**
     * 日喀则市 桑珠孜区
     * <p>
     * 藏语：གཞིས་ཀ་རྩེ་གྲོང་ཁྱེར། བསམ་འགྲུབ་རྩེ་ཆུས།<br>
     * 外语：Samzhubzê
     */
    SAMZHUBZE(SHIGATSE, DISTRICT, /* XIG.administrativeDivisionName */ "桑珠孜区", XIG),

    /**
     * 日喀则市 南木林县
     * <p>
     * 藏语：གཞིས་ཀ་རྩེ་གྲོང་ཁྱེར། རྣམ་གླིང་རྫོང་།
     */
    NAMLING(SHIGATSE, COUNTY, NAM.administrativeDivisionName, NAM),

    /**
     * 日喀则市 江孜县
     * <p>
     * 藏语：གཞིས་ཀ་རྩེ་གྲོང་ཁྱེར། རྒྱལ་རྩེ་རྫོང་།<br>
     * 外语：Gyangzê
     */
    GYANGZE(SHIGATSE, COUNTY, GYZ.administrativeDivisionName, GYZ),

    /**
     * 日喀则市 定日县
     * <p>
     * 藏语：གཞིས་ཀ་རྩེ་གྲོང་ཁྱེར། དིང་རི་རྫོང་།
     */
    TINGRI(SHIGATSE, COUNTY, TIN.administrativeDivisionName, TIN),

    /**
     * 日喀则市 萨迦县
     * <p>
     * 藏语：གཞིས་ཀ་རྩེ་གྲོང་ཁྱེར། ས་སྐྱ་རྫོང་།
     */
    SAGYA(SHIGATSE, COUNTY, SGX.administrativeDivisionName, SGX),

    /**
     * 日喀则市 拉孜县
     * <p>
     * 藏语：གཞིས་ཀ་རྩེ་གྲོང་ཁྱེར། ལྷ་རྩེ་རྫོང་།
     */
    LHATSE(SHIGATSE, COUNTY, LAZ.administrativeDivisionName, LAZ),

    /**
     * 日喀则市 昂仁县
     * <p>
     * 藏语：གཞིས་ཀ་རྩེ་གྲོང་ཁྱེར། ངམ་རིང་རྫོང་།
     */
    NGAMRING(SHIGATSE, COUNTY, NGA.administrativeDivisionName, NGA),

    /**
     * 日喀则市 谢通门县
     * <p>
     * 藏语：གཞིས་ཀ་རྩེ་གྲོང་ཁྱེར། བཞད་མཐོང་སྨོན་རྫོང་།
     */
    XAITONGMOIN(SHIGATSE, COUNTY, XTM.administrativeDivisionName, XTM),

    /**
     * 日喀则市 白朗县
     * <p>
     * 藏语：གཞིས་ཀ་རྩེ་གྲོང་ཁྱེར། པ་སྣམ་རྫོང་།
     */
    BAINANG(SHIGATSE, COUNTY, BAI.administrativeDivisionName, BAI),

    /**
     * 日喀则市 仁布县
     * <p>
     * 藏语：གཞིས་ཀ་རྩེ་གྲོང་ཁྱེར། རིན་སྤུངས་རྫོང་།
     */
    RINBUNG(SHIGATSE, COUNTY, RIN.administrativeDivisionName, RIN),

    /**
     * 日喀则市 康马县
     * <p>
     * 藏语：གཞིས་ཀ་རྩེ་གྲོང་ཁྱེར། ཁང་དམར་རྫོང་།
     */
    KANGMAR(SHIGATSE, COUNTY, KAN.administrativeDivisionName, KAN),

    /**
     * 日喀则市 定结县
     * <p>
     * 藏语：གཞིས་ཀ་རྩེ་གྲོང་ཁྱེར། གཏིང་སྐྱེས་རྫོང་།<br>
     * 外语：Dinggyê
     */
    DINGGYE(SHIGATSE, COUNTY, DIN.administrativeDivisionName, DIN),

    /**
     * 日喀则市 仲巴县
     * <p>
     * 藏语：གཞིས་ཀ་རྩེ་གྲོང་ཁྱེར། འབྲོང་པ་རྫོང་།
     */
    ZHONGBA(SHIGATSE, COUNTY, ZHB.administrativeDivisionName, ZHB),

    /**
     * 日喀则市 亚东县
     * <p>
     * 藏语：གཞིས་ཀ་རྩེ་གྲོང་ཁྱེར། གྲོ་མོ་རྫོང་།
     */
    YADONG(SHIGATSE, COUNTY, YDZ.administrativeDivisionName, YDZ),

    /**
     * 日喀则市 吉隆县
     * <p>
     * 藏语：གཞིས་ཀ་རྩེ་གྲོང་ཁྱེར། སྐྱིད་གྲོང་རྫོང་།
     */
    GYIRONG(SHIGATSE, COUNTY, GIR.administrativeDivisionName, GIR),

    /**
     * 日喀则市 聂拉木县
     * <p>
     * 藏语：གཞིས་ཀ་རྩེ་གྲོང་ཁྱེར། གཉའ་ལམ་རྫོང་།
     */
    NYALAM(SHIGATSE, COUNTY, NYA.administrativeDivisionName, NYA),

    /**
     * 日喀则市 萨嘎县
     * <p>
     * 藏语：གཞིས་ཀ་རྩེ་གྲོང་ཁྱེར། ས་དགའ་རྫོང་།
     */
    SAGA(SHIGATSE, COUNTY, SAG.administrativeDivisionName, SAG),

    /**
     * 日喀则市 岗巴县
     * <p>
     * 藏语：གཞིས་ཀ་རྩེ་གྲོང་ཁྱེར། གམ་པ་རྫོང་།
     */
    KAMBA(SHIGATSE, COUNTY, GAM.administrativeDivisionName, GAM),

    /**
     * 山南市 乃东区
     * <p>
     * 藏语：ལྷོ་ཁ་གྲོང་ཁྱེར། སྣེ་གདོང་རྫོང་།
     */
    NEDONG(SHANNAN, DISTRICT, /* NED.administrativeDivisionName */ "乃东区", NED),

    // 新增乡级（即“四级”）区域：泽当街道
    /**
     * 山南市 乃东区 泽当街道
     * <p>
     * 藏语：ལྷོ་ཁ་གྲོང་ཁྱེར། སྣེ་གདོང་རྫོང་། རྩེ་ཐང་
     */
    TSETANG(NEDONG, SUBDISTRICT, "泽当街道", null),

    /**
     * 山南市 错那市
     * <p>
     * 藏语：ལྷོ་ཁ་གྲོང་ཁྱེར། མཚོ་སྣ་རྫོང་།
     */
    TSONA(SHANNAN, COUNTY, /* CON.administrativeDivisionName */ "错那市", CON),

    /**
     * 山南市 扎囊县
     * <p>
     * 藏语：ལྷོ་ཁ་གྲོང་ཁྱེར། གྲ་ནང་རྫོང
     */
    ZHANANG(SHANNAN, COUNTY, CNG.administrativeDivisionName, CNG),

    /**
     * 山南市 贡嘎县
     * <p>
     * 藏语：ལྷོ་ཁ་གྲོང་ཁྱེར། གོང་དཀར་རྫོང
     */
    GONGGAR(SHANNAN, COUNTY, GON.administrativeDivisionName, GON),

    /**
     * 山南市 桑日县
     * <p>
     * 藏语：ལྷོ་ཁ་གྲོང་ཁྱེར། ཟངས་རི་རྫོང་།
     */
    SANGRI(SHANNAN, COUNTY, SRI.administrativeDivisionName, SRI),

    /**
     * 山南市 琼结县
     * <p>
     * 藏语：ལྷོ་ཁ་གྲོང་ཁྱེར། འཕྱོངས་རྒྱས་རྫོང
     */
    QONGGYAI(SHANNAN, COUNTY, QON.administrativeDivisionName, QON),

    /**
     * 山南市 曲松县
     * <p>
     * 藏语：ལྷོ་ཁ་གྲོང་ཁྱེར། ཆུ་གསུམ་རྫོང
     */
    QUSUM(SHANNAN, COUNTY, QUS.administrativeDivisionName, QUS),

    /**
     * 山南市 措美县
     * <p>
     * 藏语：ལྷོ་ཁ་གྲོང་ཁྱེར། མཚོ་སམད་རྫོང
     */
    COMAI(SHANNAN, COUNTY, COM.administrativeDivisionName, COM),

    /**
     * 山南市 洛扎县
     * <p>
     * 藏语：ལྷོ་ཁ་གྲོང་ཁྱེར། ལྷོ་བྲག་རྫོང
     */
    LHOZHAG(SHANNAN, COUNTY, LHX.administrativeDivisionName, LHX),

    /**
     * 山南市 加查县
     * <p>
     * 藏语：ལྷོ་ཁ་གྲོང་ཁྱེར། རྒྱ་ཚ་རྫོང
     */
    GYACA(SHANNAN, COUNTY, GYA.administrativeDivisionName, GYA),

    /**
     * 山南市 隆子县
     * <p>
     * 藏语：ལྷོ་ཁ་གྲོང་ཁྱེར། ལྷུན་རྩེ་རྫོང་།<br>
     * 外语：Lhünzê
     */
    LHUNZE(SHANNAN, COUNTY, LHZ.administrativeDivisionName, LHZ),

    /**
     * 山南市 浪卡子县
     * <p>
     * 藏语：ལྷོ་ཁ་གྲོང་ཁྱེར། སྣ་དཀར་རྩེ་རྫོང<br>
     * 外语：Nagarzê
     */
    NAGARZE(SHANNAN, COUNTY, NAX.administrativeDivisionName, NAX),

    /**
     * 林芝市 巴宜区
     * <p>
     * 藏语：ཉིང་ཁྲི་གྲོང་ཁྱེར། བྲག་ཡིབ་གྲོང་ཆུས།
     */
    BAYI(NYINGCHI, DISTRICT, /* NYI.administrativeDivisionName */ "巴宜区", NYI),

    // 新增乡级（即“四级”）区域：八一镇
    /**
     * 林芝市 巴宜区 八一镇
     * <p>
     * 藏语：ཉིང་ཁྲི་གྲོང་ཁྱེར། བྲག་ཡིབ་གྲོང་ཆུས། བྲག་ཡིབ་གྲོང།
     */
    BAYI_TOWN(BAYI, TOWNSHIP, "八一镇", null),

    /**
     * 林芝市 工布江达县
     * <p>
     * 藏语：ཉིང་ཁྲི་གྲོང་ཁྱེར། ཀོང་པོ་རྒྱ་མདའ་རྫོང་།
     */
    GONGBOGYAMDA(NYINGCHI, COUNTY, GOX.administrativeDivisionName, GOX),

    /**
     * 林芝市 米林市
     * <p>
     * 藏语：ཉིང་ཁྲི་གྲོང་ཁྱེར། སྨན་གླིང་རྫོང་།
     */
    MAINLING(NYINGCHI, COUNTY, /* MAX.administrativeDivisionName */ "米林市", MAX),

    /**
     * 林芝市 墨脱县
     * <p>
     * 藏语：ཉིང་ཁྲི་གྲོང་ཁྱེར། མེ་ཏོག་རྫོང་།<br>
     * 外语：Mêdog
     */
    MEDOG(NYINGCHI, COUNTY, MET.administrativeDivisionName, MET),

    /**
     * 林芝市 波密县
     * <p>
     * 藏语：ཉིང་ཁྲི་གྲོང་ཁྱེར། སྤོ་མེས་རྫོང<br>
     * 外语：Bomê
     */
    BOME(NYINGCHI, COUNTY, BMI.administrativeDivisionName, BMI),

    /**
     * 林芝市 察隅县
     * <p>
     * 藏语：ཉིང་ཁྲི་གྲོང་ཁྱེར། རྫ་ཡུལ་རྫོང་།
     */
    ZAYU(NYINGCHI, COUNTY, ZAY.administrativeDivisionName, ZAY),

    /**
     * 林芝市 朗县
     * <p>
     * 藏语：ཉིང་ཁྲི་གྲོང་ཁྱེར། སྣང་རྫོང་།
     */
    NANG(NYINGCHI, COUNTY, NGX.administrativeDivisionName, NGX),

    /**
     * 昌都市 卡若区
     * <p>
     * 藏语：ཆབ་མདོ་གྲོང་ཁྱེར། མཁར་རོ་ཆུས།
     */
    KARUO(CHAMDO, COUNTY, /* QAX.administrativeDivisionName */ "卡若区", QAX),

    // 新增乡级（即“四级”）区域：俄洛镇
    /**
     * 昌都市 卡若区 俄洛镇
     * <p>
     * 藏语：ཆབ་མདོ་གྲོང་ཁྱེར། མཁར་རོ་ཆུས། འགུ་རོ་
     */
    ELUO_TOWN(KARUO, TOWNSHIP, "俄洛镇", null),

    /**
     * 昌都市 江达县
     * <p>
     * 藏语：ཆབ་མདོ་གྲོང་ཁྱེར། འཇོ་མདའ་རྫོང
     */
    JOMDA(CHAMDO, COUNTY, JOM.administrativeDivisionName, JOM),

    /**
     * 昌都市 贡觉县
     * <p>
     * 藏语：ཆབ་མདོ་གྲོང་ཁྱེར། གོ་འཇོ་རྫོང
     */
    GONJO(CHAMDO, COUNTY, KON.administrativeDivisionName, KON),

    /**
     * 昌都市 类乌齐县
     * <p>
     * 藏语：ཆབ་མདོ་གྲོང་ཁྱེར། རི་བོ་ཆེ་རྫོང
     */
    RIWOCHE(CHAMDO, COUNTY, RIW.administrativeDivisionName, RIW),

    /**
     * 昌都市 丁青县
     * <p>
     * 藏语：ཆབ་མདོ་གྲོང་ཁྱེར། སྟེང་ཆེན་རྫོང<br>
     * 外语：Dêngqên
     */
    DENGQEN(CHAMDO, COUNTY, DEN.administrativeDivisionName, DEN),

    /**
     * 昌都市 察雅县
     * <p>
     * 藏语：ཆབ་མདོ་གྲོང་ཁྱེར། བྲག་གཡབ་རྫོང
     */
    ZHAGYAB(CHAMDO, COUNTY, CHA.administrativeDivisionName, CHA),

    /**
     * 昌都市 八宿县
     * <p>
     * 藏语：ཆབ་མདོ་གྲོང་ཁྱེར། དཔའ་ཤོད་རྫོང
     */
    PASHO(CHAMDO, COUNTY, BAX.administrativeDivisionName, BAX),

    /**
     * 昌都市 左贡县
     * <p>
     * 藏语：ཆབ་མདོ་གྲོང་ཁྱེར། མཛོ་སྒང་རྫོང
     */
    ZOGANG(CHAMDO, COUNTY, ZOX.administrativeDivisionName, ZOX),

    /**
     * 昌都市 芒康县
     * <p>
     * 藏语：ཆབ་མདོ་གྲོང་ཁྱེར། སྨར་ཁམས་རྫོང
     */
    MARKAM(CHAMDO, COUNTY, MAN.administrativeDivisionName, MAN),

    /**
     * 昌都市 洛隆县
     * <p>
     * 藏语：ཆབ་མདོ་གྲོང་ཁྱེར། ལྷོ་རོང་རྫོང
     */
    LHORONG(CHAMDO, COUNTY, LHO.administrativeDivisionName, LHO),

    /**
     * 昌都市 边坝县
     * <p>
     * 藏语：ཆབ་མདོ་གྲོང་ཁྱེར། དཔལ་འབར་རྫོང
     */
    BANBAR(CHAMDO, COUNTY, BAN.administrativeDivisionName, BAN),

    /**
     * 那曲市 色尼区
     * <p>
     * 藏语：ནག་ཆུ་གྲོང་ཁྱེར། གསེར་རྙེད་ཆུས།
     */
    SENI(NAGQU, DISTRICT, /* NAG.administrativeDivisionName */ "色尼区", NAG),

    /**
     * 那曲市 嘉黎县
     * <p>
     * 藏语：ནག་ཆུ་གྲོང་ཁྱེར། ལྷ་རི་རྫོང
     */
    LHARI(NAGQU, COUNTY, LHR.administrativeDivisionName, LHR),

    /**
     * 那曲市 比如县
     * <p>
     * 藏语：ནག་ཆུ་གྲོང་ཁྱེར། འབྲི་རུ་རྫོང
     */
    BIRU(NAGQU, COUNTY, BRU.administrativeDivisionName, BRU),

    /**
     * 那曲市 聂荣县
     * <p>
     * 藏语：ནག་ཆུ་གྲོང་ཁྱེར། གཉན་རོང་རྫོང
     */
    NYAINRONG(NAGQU, COUNTY, NRO.administrativeDivisionName, NRO),

    /**
     * 那曲市 安多县
     * <p>
     * 藏语：ནག་ཆུ་གྲོང་ཁྱེར། ཨ་མདོ་རྫོང་
     */
    AMDO(NAGQU, COUNTY, AMD.administrativeDivisionName, AMD),

    /**
     * 那曲市 申扎县
     * <p>
     * 藏语：ནག་ཆུ་གྲོང་ཁྱེར། ཤན་རྩ་རྫོང
     */
    XAINZA(NAGQU, COUNTY, XZX.administrativeDivisionName, XZX),

    /**
     * 那曲市 索县
     * <p>
     * 藏语：ནག་ཆུ་གྲོང་ཁྱེར། སོག་རྫོང་
     */
    SOG(NAGQU, COUNTY, AdministrativeDivision.SOG.administrativeDivisionName, AdministrativeDivision.SOG),

    /**
     * 那曲市 班戈县
     * <p>
     * 藏语：ནག་ཆུ་གྲོང་ཁྱེར། དཔལ་མགོན་རྫོང
     */
    BAINGOIN(NAGQU, COUNTY, BGX.administrativeDivisionName, BGX),

    /**
     * 那曲市 巴青县
     * <p>
     * 藏语：ནག་ཆུ་གྲོང་ཁྱེར། སྦྲ་ཆེན་རྫོང<br>
     * 外语：Baqên
     */
    BAQEN(NAGQU, COUNTY, BQE.administrativeDivisionName, BQE),

    /**
     * 那曲市 尼玛县
     * <p>
     * 藏语：ནག་ཆུ་གྲོང་ཁྱེར། ཉི་མ་རྫོང
     */
    NYIMA(NAGQU, COUNTY, NYX.administrativeDivisionName, NYX),

    /**
     * 那曲市 双湖县
     * <p>
     * 藏语：ནག་ཆུ་གྲོང་ཁྱེར། མཚོ་གཉིས་རྫོང་།
     * <p>
     * 注意：本代码中双湖县采用的{@link AdministrativeDivision#XZX 申扎县}行政区划
     * <p>
     * 双湖所辖地域原属于申扎县，为中国海拔最高县。
     * <ul>
     *   <li>双湖办事处的前身是1973年成立的申扎县加林工作组。1974年初，加林工作组开赴无人区，在加林山脚下的俄东沟（今尼玛县荣玛乡境内）安营扎寨。因加林山而得名。</li>
     *   <li>1975年底，西藏自治区党委批准申扎县尼玛区、吉瓦区的3个乡（即尼玛区北措折乡，吉瓦区俄久美、俄久道乡）、文部区的1个乡（即来多强玛乡）、申扎区的1个公社（即嘎措公社），以及班戈县的色瓦区（包括色瓦、多玛、巴岭、买玛4个乡）的牧民北迁无人区，组建双湖办事处。</li>
     *   <li>1976年2月，设立双湖办事处。直接隶属那曲地区。那曲地区革委会主任洛桑丹珍兼任办事处党委书记、主任。至1977年底，双湖办事处基本完成牧民及牲畜的北迁与安置工作。</li>
     *   <li>1983年起，双湖办事处所辖地域改属于新设立的尼玛县，但双湖办事处继续由那曲地区直接领导。</li>
     *   <li>1993年8月，双湖办事处改称尼玛县双湖特别区，但双湖特别区继续由那曲地区直接领导。</li>
     *   <li>2012年11月正式设立为双湖县。县政府驻多玛乡索嘎鲁玛社区，海拔4938米。</li>
     * </ul>
     */
    SHUANGHU(NAGQU, COUNTY, /* XZX.administrativeDivisionName */ "双湖县", XZX),

    /**
     * 阿里地区 普兰县
     * <p>
     * 藏语：མངའ་རིས་ས་ཁུལ། སྤུ་ཧྲེང་རྫོང་།
     */
    PURANG(NGARI, COUNTY, BUR.administrativeDivisionName, BUR),

    /**
     * 阿里地区 札达县
     * <p>
     * 藏语：མངའ་རིས་ས་ཁུལ། རྩ་མདའ་རྫོང་།
     */
    ZANDA(NGARI, COUNTY, ZAN.administrativeDivisionName, ZAN),

    /**
     * 阿里地区 噶尔县
     * <p>
     * 藏语：མངའ་རིས་ས་ཁུལ། སྒར་རྫོང་།
     */
    GAR(NGARI, COUNTY, AdministrativeDivision.GAR.administrativeDivisionName, AdministrativeDivision.GAR),

    // 新增乡级（即“四级”）区域：狮泉河镇
    /**
     * 阿里地区 噶尔县 狮泉河镇
     * <p>
     * 藏语：མངའ་རིས་ས་ཁུལ། སྒར་རྫོང་། སེང་གེ་ཁ་འབབ་
     */
    SHIQUANHE(GAR, TOWNSHIP, "狮泉河镇", null),

    /**
     * 阿里地区 日土县
     * <p>
     * 藏语：མངའ་རིས་ས་ཁུལ། རུ་ཐོག་རྫོང་།
     */
    RUTOG(NGARI, COUNTY, RUT.administrativeDivisionName, RUT),

    /**
     * 阿里地区 革吉县
     * <p>
     * 藏语：མངའ་རིས་ས་ཁུལ། དགེ་རྒྱས་རྫོང་།<br>
     * 外语：Gê'gyai
     */
    GEGYAI(NGARI, COUNTY, GEG.administrativeDivisionName, GEG),

    /**
     * 阿里地区 改则县
     * <p>
     * 藏语：མངའ་རིས་ས་ཁུལ། སྒེར་རྩེ་རྫོང་།
     */
    GERTSE(NGARI, COUNTY, GER.administrativeDivisionName, GER),

    /**
     * 阿里地区 措勤县
     * <p>
     * 藏语：མངའ་རིས་ས་ཁུལ། མཚོ་ཆེན་རྫོང་།<br>
     * 外语：Coqên
     */
    COQEN(NGARI, COUNTY, COQ.administrativeDivisionName, COQ);

    // TODO: 四级、五级区域未定义，待完善

    /**
     * 父级区域（即所属区域）
     * <p>
     * 若该属性为“null”，则表示该区域为中华人民共和国顶级区域
     */
    public final Region parent;

    /**
     * 区域类别
     */
    public final Category category;

    /**
     * 区域中文名
     */
    public final String chineseName;

    /**
     * 行政区划
     * <p>
     * 若该区域为行政区划，则会有该属性
     */
    public final AdministrativeDivision administrativeDivision;

    /**
     * 查找一组指定级别的区域
     *
     * @param level 查找级别
     * @return 一组区域
     */
    public static Set<Region> findRegions(Level level) {
        Set<Region> set = new LinkedHashSet<>();
        for (Region r : Region.values()) if (r.category.level == level) set.add(r);
        return set;
    }

    /**
     * 查找一组归属指定区域的指定级别区域
     *
     * @param region 指定区域
     * @param level  查找级别
     * @return 一组区域
     */
    public static Set<Region> findRegions(Region region, Level level) {
        Set<Region> set = new LinkedHashSet<>();
        for (Region r : findRegions(level)) if (r.isBelongTo(region)) set.add(r);
        return set;
    }

    /**
     * 是否为一级区域
     *
     * @return 当当前区域枚举为一级区域是返回true，否则返回false
     */
    public boolean isProvincialLevelDivision() {
        return this.category.level == Level.PROVINCIAL;
    }

    /**
     * 是否为一级区域
     *
     * @return 当当前区域枚举为一级区域是返回true，否则返回false
     */
    public boolean is1stLevelDivision() {
        return isProvincialLevelDivision();
    }

    /**
     * 是否为一级区域
     *
     * @return 当当前区域枚举为一级区域是返回true，否则返回false
     */
    public boolean isFirstLevelDivision() {
        return isProvincialLevelDivision();
    }

    /**
     * 是否为二级区域
     *
     * @return 当当前区域枚举为二级区域是返回true，否则返回false
     */
    public boolean isPrefecturalLevelDivision() {
        return this.category.level == Level.PREFECTURAL;
    }

    /**
     * 是否为二级区域
     *
     * @return 当当前区域枚举为二级区域是返回true，否则返回false
     */
    public boolean is2ndLevelDivision() {
        return isPrefecturalLevelDivision();
    }

    /**
     * 是否为二级区域
     *
     * @return 当当前区域枚举为二级区域是返回true，否则返回false
     */
    public boolean isSecondLevelDivision() {
        return isPrefecturalLevelDivision();
    }

    /**
     * 是否为三级区域
     *
     * @return 当当前区域枚举为三级区域是返回true，否则返回false
     */
    public boolean isCountyLevelDivision() {
        return this.category.level == Level.COUNTY;
    }

    /**
     * 是否为三级区域
     *
     * @return 当当前区域枚举为三级区域是返回true，否则返回false
     */
    public boolean is3rdLevelDivision() {
        return isCountyLevelDivision();
    }

    /**
     * 是否为三级区域
     *
     * @return 当当前区域枚举为三级区域是返回true，否则返回false
     */
    public boolean isThirdLevelDivision() {
        return isCountyLevelDivision();
    }

    /**
     * 是否为四级区域
     *
     * @return 当当前区域枚举为四级区域是返回true，否则返回false
     */
    public boolean isTownshipLevelDivision() {
        return this.category.level == Level.TOWNSHIP;
    }

    /**
     * 是否为四级区域
     *
     * @return 当当前区域枚举为四级区域是返回true，否则返回false
     */
    public boolean is4thLevelDivision() {
        return isTownshipLevelDivision();
    }

    /**
     * 是否为四级区域
     *
     * @return 当当前区域枚举为四级区域是返回true，否则返回false
     */
    public boolean isFourthLevelDivision() {
        return isTownshipLevelDivision();
    }

    /**
     * 是否为五级区域
     *
     * @return 当当前区域枚举为五级区域是返回true，否则返回false
     */
    public boolean isBasicLevelDivision() {
        return this.category.level == Level.BASIC;
    }

    /**
     * 是否为五级区域
     *
     * @return 当当前区域枚举为五级区域是返回true，否则返回false
     */
    public boolean is5thLevelDivision() {
        return isBasicLevelDivision();
    }

    /**
     * 是否为五级区域
     *
     * @return 当当前区域枚举为五级区域是返回true，否则返回false
     */
    public boolean isFifthLevelDivision() {
        return isBasicLevelDivision();
    }

    /**
     * 判断当前区域是否属于指定区域
     *
     * @param parent 指定区域
     * @return 当当前区域属于指定区域是返回true，否则返回false
     */
    public boolean isBelongTo(Region parent) {
        if (this.parent == null) return false;
        if (this.parent == parent) return true;
        return this.parent.isBelongTo(parent);
    }

}

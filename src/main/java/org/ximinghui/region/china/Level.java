package org.ximinghui.region.china;

import lombok.AllArgsConstructor;

/**
 * 地区级别
 */
@AllArgsConstructor
public enum Level {

    /**
     * 一级
     */
    PROVINCIAL("省级"),

    /**
     * 二级
     */
    PREFECTURAL("地级"),

    /**
     * 三级
     */
    COUNTY("县级"),

    /**
     * 四级
     */
    TOWNSHIP("乡级"),

    /**
     * 五级
     */
    BASIC("村级");

    /**
     * 地区级别中文名
     */
    public final String chineseName;

}
